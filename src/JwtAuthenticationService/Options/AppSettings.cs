﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JwtAuthenticationService.Options
{
    public class AppSettings
    {
        public string? Secret { get; set; }
        public int RefreshTokenTtl { get; set; }
        public int JwtTokenTtl { get; set; }
    }
}
