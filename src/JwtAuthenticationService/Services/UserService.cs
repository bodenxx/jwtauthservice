﻿using JwtAuthenticationService.Authorization;
using JwtAuthenticationService.Data;
using JwtAuthenticationService.Entities;
using JwtAuthenticationService.Exceptions;
using JwtAuthenticationService.Extensions;
using JwtAuthenticationService.Models;
using JwtAuthenticationService.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace JwtAuthenticationService.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IJwtUtils _jwtUtils;
        private readonly AppSettings _appSettings;

        public UserService(UserManager<User> userManager, IJwtUtils jwtUtils, IOptions<AppSettings> appSettings)
        {
            _userManager = userManager;
            _jwtUtils = jwtUtils;
            _appSettings = appSettings.Value;
        }

        public async Task<RegistrationResponse> SignUpAsync(RegistrationRequest model, string ipAddress)
        {
            User newUser = new User
            {
                UserName = model.UserName,
                Email = model.Email
            };

            await CreateUserAsync(newUser, model.Password!);

            var jwtToken = _jwtUtils.GenerateJwtToken(newUser);
            var refreshToken = _jwtUtils.GenerateRefreshToken(ipAddress);
            newUser.RefreshTokens!.Add(refreshToken);

            await UpdateUserAsync(newUser);

            return new RegistrationResponse(newUser, jwtToken, refreshToken.Token!);
        }

        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model, string ipAddress)
        {
            var user = await _userManager.FindByNameAsync(model.Username);

            await ValidateUserAsync(user, model.Password);

            RemoveOldRefreshTokens(user);

            var jwtToken = _jwtUtils.GenerateJwtToken(user);
            var refreshToken = _jwtUtils.GenerateRefreshToken(ipAddress);
            user.RefreshTokens!.Add(refreshToken);

            await UpdateUserAsync(user);

            return new AuthenticateResponse(user, jwtToken, refreshToken.Token!);
        }

        public async Task<AuthenticateResponse> RefreshTokenAsync(string token, string ipAddress)
        {
            var user = await _userManager.FindByRefreshTokenAsync(token);
            var refreshToken = user!.RefreshTokens!.Single(x => x.Token == token);

            if (refreshToken.IsRevoked)
            {
                RevokeDescendantRefreshTokens(refreshToken, user, ipAddress, $"Attempted reuse of revoked ancestor token: {token}");

                await UpdateUserAsync(user);
            }

            if (!refreshToken.IsActive)
                throw new AppException("Invalid token");

            RemoveOldRefreshTokens(user);

            var jwtToken = _jwtUtils.GenerateJwtToken(user);
            var newRefreshToken = RotateRefreshToken(refreshToken, ipAddress);
            user.RefreshTokens!.Add(newRefreshToken);

            await UpdateUserAsync(user);

            return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token!);
        }

        public async Task RevokeTokenAsync(string token, string ipAddress)
        {
            var user = await _userManager.FindByRefreshTokenAsync(token);
            var refreshToken = user!.RefreshTokens!.Single(x => x.Token == token);

            if (!refreshToken.IsActive)
                throw new AppException("Invalid token");

            RevokeRefreshToken(refreshToken, ipAddress, "Revoked without replacement");

            await UpdateUserAsync(user);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _userManager.Users.ToListAsync();
        }

        public async Task<User> GetByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) throw new KeyNotFoundException("User not found");
            return user;
        }

        private RefreshToken RotateRefreshToken(RefreshToken refreshToken, string ipAddress)
        {
            var newRefreshToken = _jwtUtils.GenerateRefreshToken(ipAddress);
            RevokeRefreshToken(refreshToken, ipAddress, "Replaced by new token", newRefreshToken.Token!);
            return newRefreshToken;
        }

        private void RemoveOldRefreshTokens(User user)
        {
            user.RefreshTokens!.RemoveAll(x =>
                !x.IsActive &&
                x.Created.AddDays(_appSettings.RefreshTokenTtl) <= DateTime.UtcNow);
        }

        private void RevokeDescendantRefreshTokens(RefreshToken refreshToken, User user, string ipAddress, string reason)
        {
            if (!string.IsNullOrEmpty(refreshToken.ReplacedByToken))
            {
                var childToken = user.RefreshTokens!.SingleOrDefault(x => x.Token == refreshToken.ReplacedByToken);
                if (childToken!.IsActive)
                    RevokeRefreshToken(childToken, ipAddress, reason);
                else
                    RevokeDescendantRefreshTokens(childToken, user, ipAddress, reason);
            }
        }

        private void RevokeRefreshToken(RefreshToken token, string ipAddress, string reason = null, string replacedByToken = null)
        {
            token.Revoked = DateTime.UtcNow;
            token.RevokedByIp = ipAddress;
            token.ReasonRevoked = reason;
            token.ReplacedByToken = replacedByToken;
        }

        private async Task CreateUserAsync(User user, string password)
        {
            var registrationResult = await _userManager.CreateAsync(user, password);
            if (!registrationResult.Succeeded)
                throw new AppException($"User registration failed! Please check user details and try again. {registrationResult.Errors.ToList()[0].Description}");
        }

        private async Task UpdateUserAsync(User user)
        {
            user.SecurityStamp = Guid.NewGuid().ToString();
            var updateResult = await _userManager.UpdateAsync(user);
            if (!updateResult.Succeeded)
                throw new AppException($"User updating failed! Please check user details and try again. {updateResult.Errors.ToList()[0].Description}");
        }

        private async Task ValidateUserAsync(User? user, string? modelPassword)
        {
            if (user == null || !await _userManager.CheckPasswordAsync(user, modelPassword))
                throw new AppException("Username or password is incorrect!");

            if (!user.IsEnabled)
                throw new AppException("User is disabled!");
        }
    }
}
