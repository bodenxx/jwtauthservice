﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JwtAuthenticationService.Entities;
using JwtAuthenticationService.Models;

namespace JwtAuthenticationService.Services
{
    public interface IUserService
    {
        Task<RegistrationResponse> SignUpAsync(RegistrationRequest model, string ipAddress);
        Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model, string ipAddress);
        Task<AuthenticateResponse> RefreshTokenAsync(string token, string ipAddress);
        Task RevokeTokenAsync(string token, string ipAddress);
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> GetByIdAsync(string id);
    }
}
