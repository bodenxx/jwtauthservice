﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;

namespace JwtAuthenticationService.Entities
{
    public class User : IdentityUser
    {
        public bool IsEnabled { get; set; } = true;

        [JsonIgnore] public List<RefreshToken>? RefreshTokens { get; set; } = new List<RefreshToken>();
    }
}
