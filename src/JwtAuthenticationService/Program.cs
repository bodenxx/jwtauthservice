using System.Data;
using System.Data.SqlClient;
using System.Text;
using JwtAuthenticationService.Authorization;
using JwtAuthenticationService.Data;
using JwtAuthenticationService.Entities;
using JwtAuthenticationService.Middleware;
using JwtAuthenticationService.Options;
using JwtAuthenticationService.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;


var builder = WebApplication.CreateBuilder(args);

var connStringBuilder = new SqlConnectionStringBuilder(builder.Configuration.GetConnectionString("Default"))
 {
     Password = builder.Configuration["DbConnection:Password"],
     UserID = builder.Configuration["DbConnection:User"]
 };

builder.Services.AddDbContext<AppDbContext>(options =>
{
    options.UseSqlServer(connStringBuilder.ConnectionString);
});

builder.Services.AddIdentity<User, IdentityRole>()
    .AddEntityFrameworkStores<AppDbContext>()
    .AddDefaultTokenProviders();


builder.Services.Configure<IdentityOptions>(builder.Configuration.GetSection("IdentitySettings"));

builder.Services.AddCors();
// Add services to the container
builder.Services.AddControllers();

builder.Services.Configure<AppSettings>(options =>
{
    options.Secret = builder.Configuration["TokenSecret"];
    options.RefreshTokenTtl = builder.Configuration.GetValue<int>("AppSettings:RefreshTokenTtl");
    options.JwtTokenTtl = builder.Configuration.GetValue<int>("AppSettings:JwtTokenTtl");
});

builder.Services.AddScoped<IJwtUtils, JwtUtils>();
builder.Services.AddScoped<IUserService, UserService>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(x => x
    .SetIsOriginAllowed(origin => true)
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowCredentials());

app.UseHttpsRedirection();

// global error handler
app.UseMiddleware<ErrorHandlerMiddleware>();

// custom jwt auth middleware
app.UseMiddleware<JwtMiddleware>();
app.MapControllers();

app.Run();
