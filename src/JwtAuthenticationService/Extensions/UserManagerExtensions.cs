﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JwtAuthenticationService.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace JwtAuthenticationService.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<User?> FindByRefreshTokenAsync(this UserManager<User> userManager, string token)
        {
            return await userManager.Users.SingleOrDefaultAsync(u => u.RefreshTokens!.Any(t => t.Token == token));
        }
    }
}
